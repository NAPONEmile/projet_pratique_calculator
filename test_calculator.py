import unittest

import calculator


class TestUtils(unittest.TestCase):
    def test_addit(self):
        self.assertEqual(calculator.addit(2,3), 5)
        self.assertEqual(calculator.addit(-2,-6), -8)
        self.assertNotEqual(calculator.addit(2,0), 4)
        
    def test_sub(self):
        self.assertEqual(calculator.sub(2,3), -1)
        self.assertEqual(calculator.sub(-2,1), -3)
        self.assertNotEqual(calculator.sub(2,0), 4)
        
    def test_div(self):
        self.assertEqual(calculator.div(4,2), 2)
        self.assertEqual(calculator.div(2,1), 2)
        self.assertNotEqual(calculator.div(2,0), 4)
        
     
    def test_mult1(self):
        self.assertEqual(calculator.mult1(2,3), 6)
        self.assertEqual(calculator.mult1(-2,-6), 12)
        self.assertNotEqual(calculator.mult1(2,0), 4)
    def test_mult2(self):
        self.assertEqual(calculator.mult2(2,3), 6)
        self.assertEqual(calculator.mult2(-2,-6), 12)
        self.assertNotEqual(calculator.mult2(2,0), 4)
    def test_mult1(self):
        self.assertEqual(calculator.mult3(2,3), 6)
        self.assertEqual(calculator.mult3(-2,-6), 12)
        self.assertNotEqual(calculator.mult3(2,0), 4)
    

if __name__ == '__main__':
    unittest.main()
